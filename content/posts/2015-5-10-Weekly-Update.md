Title: Update (10-5-2015)
Date: 2015-5-10
Category: Update

Additional materials purchased. Need some copper tape, for sensors, 0.1" headers
to facilitate making breakout boards. Additionally, purchased pH strips and
calibration fluid to help construct low cost pH sensors.

Also, required some bags that were made. Hopefully those, along with two other
panels that have been made can be sold soon.

I'm also reopening the inventory project, and procedural project. As I'm having
some issues with the sensors, these are nice, productive breaks.

Tim
