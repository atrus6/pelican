Catnip
######
:date: 2018-06-07 00:42
:author: bearsif
:category: Materials
:slug: catnip
:status: published

**About**

Catnip is the most familier plant used for cat enjoyment. Also called catmint, about `70% of cats enjoy it <https://www.gwern.net/Catnip>`__.

Can be dried and saved for later, as well as buds saved for maximum cat enjoyment.

**Growing**

Catnip enjoys well draining soil, and moderate sunlight. Does extremely well being grown via hydroponics. Enjoys a wide range of ph from 6.5 to 7.8. In soil the cat likes approximately 25x25 cm of space per plant.

Very flexible, and very hardy.

**Propagation**

Catnip blooms from July through October. After the flowers mature, seeds form. I am not entirely sure if the blooming and seeding process is time or temperature dependant. Most likely both.

Seeds take 7-10 days to germinate. It can be easily started indoors due to the plants flexibility.

**Sustainability**

Catnip is essentially a weed. It will grow in most conditions, and loves to spread. Easy to grow, dry and prepare for all cats. Uses a similar nutrient blend as any lettuce, so any hydroponic project will be successful as well.

**Learn More**

http://herbgardening.com/growingcatnip.htm

http://herbgardening.com/howtogrowherbsinhyrdoponics.htm

https://www.gardeningknowhow.com/edible/herbs/catnip/growing-catnip.htm

https://www.thespruce.com/how-to-grow-catnip-plants-2132328
