1/4" HR Valve Adapter
#####################
:date: 2018-08-26 19:57
:author: bearsif
:category: Product Updates
:slug: 1-4-hr-valve-adapter
:status: published

So, I have a friend who needed an HR Valve that was useful...if you search online for HR Valve adapters, all you find are ones with a 3/4"ish barb on it. Which, if you are wanting to use an air compressor, you have to have quite a collection of different fittings and adapters to connect.

|image0|\ |image1|

Well, we found one on `thingiverse <https://www.thingiverse.com/thing:2081040>`__ and it worked mostly well. But at 99% scale, it fits perfectly!

I'm also going to sell printed ones here, and on Etsy, Amazon and eBay. Because why not? It was printed at:

-  0.99 scale.
-  20% infil
-  0.2mm layer height
-  using PLA plastic

So if you have a printer of your own, print it instead of buying! Otherwise I'm selling them at $10. Mine include pretty packaging though, so if you'd like that, please buy!

.. |image0| image:: https://worldsproject.org/wp-content/uploads/2018/08/out-300x300.jpg
   :class: alignnone size-medium wp-image-137
   :width: 300px
   :height: 300px
.. |image1| image:: https://worldsproject.org/wp-content/uploads/2018/08/bagged-300x300.jpg
   :class: alignnone size-medium wp-image-138
   :width: 300px
   :height: 300px
