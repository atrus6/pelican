Title: Spoonflower Gimp and Inkscape Palette
Date: 2018-07-17
Author: bearsif
Category: FOSS 

I've been playing around with some of [Spoonflower] (http://spoonflower.com) fabrics. One of the small challenges that I've had is matching the colors they've picked as their fabric palette. They have a palette file (a .ase, used for Adobe products), but since I try my best to use FOSS, I can't directly use those. I tend to use either GIMP or Inkscape for all my graphical work. Most of the convertors I've found are old and unmaintained, so they didn't work well. Then I found [gpick[(http://gpick.org), which worked wonderfully! Certainly saved me the trouble of having to write something that converted from ase to gpl.

But, if you don't feel like downloading it, I've also attached the converted Spoonflower palette!

[GIMP Inkscape Spoonflower Colormap]({static}/files/spoonflower_colormap.gpl)
