Kratky Hydroponic method in Mason Jars
######################################
:date: 2018-08-30 03:08
:author: bearsif
:category: Hydroponics
:slug: kratky-hydroponic-method-in-mason-jars
:status: published

I've wanted to try out some different hydroponic methods. One of them was the Kratky method. No pumps nor recirculation at all. Basically just some contained water and light. Awesome! Sounds simple.

So, to simplify things, I've went and painted some mason jars black, and printed out some net pots to hold some catnip plants. I've started some catnip in my planters outside, and I'm hoping to get some growing inside as well. I'd love to put my own catnip inside of the pillows I sell.

The net pots I got from `thingiverse <https://www.thingiverse.com/thing:790339>`__ and for the parameters I came up with the following STL: `Mason Jar Netpot <https://worldsproject.org/wp-content/uploads/2018/08/parametric-net-pot_mason_jar1.stl>`__

Print one out today! Grow some plants!

 
