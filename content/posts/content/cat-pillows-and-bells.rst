Cat pillows and bells.
######################
:date: 2018-06-25 22:54
:author: bearsif
:category: Product Updates
:slug: cat-pillows-and-bells
:status: published

New option!

Some ideas are obvious after they've been thought, including the fact that cats love jingly things!

But their owners don't always, and that's why it's an optional add on to all cat pillows! If you're cat loves bells, but you don't, you don't have to order them with it. We'll keep it between us.
