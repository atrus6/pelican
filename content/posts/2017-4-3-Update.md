Title: Weekly Update (3-4-2017)
Date: 2017-04-03
Category: Update
Tags: etsy circle_knit ipfs

New Products!

We have some earrings up for sale, along with some bulbasaur planter/thing holders. They're all very cute!

[Bulbasaur Pencil Holder and Hlanter](https://www.etsy.com/listing/522894083/te)
[Green Flower Earrings](https://www.etsy.com/listing/519940851/)
[Orange Flower Earrings](https://www.etsy.com/listing/519941461/)
[Green and Blue Flower Earrings](https://www.etsy.com/listing/504184394/)

Ive also added an Instagram. Because many of the items are visual in nature, having pictures is quite nice, and I do have to admit, the filters are kinda fun!

[Instagram](https://www.instagram.com/worldsproject/)

The knitting needles have shipped! I can't believe that I forgot to buy them, so that kinda stalled assembly. There have been some minor build issues during assembly, with items juuussssttt being slightly off. I'm not sure what parts the issue lies with, but I've been documenting everything as I go along, so hopefully we see a nice big picture at the end.

Thanks for reading!
