Title: Open Source must focus on money.
Date: 2019-4-1
Category: FOSS

[Recently](https://news.ycombinator.com/item?id=19431444) there has been talk of
how Open Source Software isn't designed to make money, plus there has been talk in
the past of how Open Source [_shouldn't_ be making money.](https://dhh.dk/2013/the-perils-of-mixing-open-source-and-money.html)

This is not a sustainable practice, as now we have larger companies piggybacking
off of this existing work, making literal billions, while not truly contributing
back.

[Heartbleed](https://en.wikipedia.org/wiki/Heartbleed) comes to mind. Two people
working on this software, that larger entities used to attain literal billions,
was an essential portion of their business practices, and they got _nothing_.

Sure, their situation has improved, but how many others are out there?
This situation needs to change. FLOSS needs resources to maintain itself.
