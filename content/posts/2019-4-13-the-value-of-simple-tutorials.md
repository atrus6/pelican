Title: The value of simple tutorials.
Date: 15-4-2019
Tags: foss, learning, harp
Category: Other

Last week I wrote a tutorial on how to make a cat pillow. It's a simple tutorial on something that is admittedly pretty basic. It is a pillow after all.

But writing that was inspired by my searches for fabric needed for a cat bed. I was curious to see what fabrics people were using, so I looked up some tutorials on different cat beds.

I had a general idea of what to use, but, maybe there was something different? And just about all of them described the materials as just... Fabric.

Which isn't helpful at all! What if I was a beginner, and had no clue? Canvas? Felt? Fleece? Denim? Is there some drawback I don't know about? Experimentation is nice, and certainly provides a learning experience, but sometimes I just want to make a cat bed, not further the field of cat preference material science.

This all draws back to the motivations of HARP. Clear, specified materials, and clear, and specified instructions. Want to go off the beaten path? Break the rules. Just want something made as is? Follow the directions.

Directions should be clear enough to go from nothing to product. 