Title: Weekly Update (1-3-2015)
Date: 2015-3-1
Category: Update

Some quick highlights of items accomplished over the week.

1. Creation of this blog.
2. Ingredient scripts, to clean up scripts.
3. Implementation of new spiders.
