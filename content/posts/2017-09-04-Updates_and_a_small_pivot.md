Title: Updates and a small pivot.
Date: 2017-09-04
Category: Update
tags: hydroponics etsy 3dprinting

It's been a while since there has been an update. Silent but not motionless.
There's been a small pivot from hydroponics. Due to high energy costs, it's not very
profitable to grow and produce just about any food crop. The only real option is something
like pot. Sadly, I don't really have an interest in drugs, only food, so that's out
of the question. Plus, it's a crowded space, and that's never any fun. So, I've been
taking my plant systems and slowly converting them to growing a variety of mushrooms.

A crop is starting, not using my hardware, but more traditional methods, as I've never
grown any mushrooms before, and I'd like to get some experience doing it the traditional
way, before I start changing things up.

I've always hated the name "AquaCedar", and I'll hopefully be searching for a new name
that I like.

Etsy has been pretty successful, I've been shipping out lots of cat pillows. It's not the
most glamorous work, but it buys mushroom kits, so who am I to complain? Plus, they are
super cute. I've been trying to work more towards marketing my stuff. It's pointless
if it just sits there, unsold.

I've finally started writing code for my SKDB redo. I've called it Harp, short for
HARdware Packager. It's a decent chunk of work, but hopefully we'll get something
up in running in a bit. Lots more to do in that department.

I have some repairs to do to the knitting machine, but hopefully I'll have it running and
pumping out scarves for the winter, and charity.
