Title: Recent work.
Date: 2019-3-28
Category: Update

Work has slowed a bit on HARP, mostly due to lack of real 'projects' to place into
it. Legos are fine, but they aren't real items, and I'd like to have actual utility
to make it worth using HARP.

Thus, KittyKube was born. A DIY Cat tree builder, with the option to have it printed
and shipped to you. All items are open sourced, meaning if you have means (a 
sewing machine, 3d printer, and pipe cutter), you can build your own, not dependant
on others.

Of course, the whole premise around WorldsProject is that we work more efficently
when working with others, and that most people don't want to maintain a sewing
machine, 3d printer and pipe cutter. Information is free, atoms cost money.
