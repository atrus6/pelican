Title: Weekly Update (8-3-2015)
Date: 2015-3-8
Category: Update

What was accomplished for the past week.

1. Upgraded the Recipe database to have cleaner, more ordered directions.
2. Remove text size restrictions on many fields of the Recipe Database.
