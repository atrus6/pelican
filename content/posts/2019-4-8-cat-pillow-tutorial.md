Title: How to make a simple cat pillow.
Date: 2019-04-08
Category: Tutorial
Tags: cats catnip tutorial

Making a catnip pillow that your cat will love is super easy. All it takes is a small section of fabric, stuffing, catnip (of course!) and a needle and thread.

First start with a 4" by 7" piece of fabric. I generally use 100% cotton fabric. 

[![Step One]({static}/files/cat_pillow_tutorial/thumb-1.jpg)]({static}/files/cat_pillow_tutorial/step1.jpg)

It gets folded in half, wrong side out.

[![Step Two]({static}/files/cat_pillow_tutorial/thumb-2.jpg)]({static}/files/cat_pillow_tutorial/step2.jpg)

Then, we sew 3 and a half sides around.

[![Step Three]({static}/files/cat_pillow_tutorial/thumb-3.jpg)]({static}/files/cat_pillow_tutorial/step3.jpg)

Any type of straight stitch is fine. Typically the fabric fails before the stitching does. 

Fold it right side out

[![Step Four]({static}/files/cat_pillow_tutorial/thumb-4.jpg)]({static}/files/cat_pillow_tutorial/step4.jpg)

Now we add some catnip! Typically, for normally stuffed pillow, I use three (heaping!) scoops. If you're feeling extra generous, fill it all up with catnip, and skip the stuffing step. Your cat will love that! 💏

[![Step Five]({static}/files/cat_pillow_tutorial/thumb-5.jpg)]({static}/files/cat_pillow_tutorial/step5.jpg)

If your pillow isn't stuffed to the brim with catnip, it's time to add stuffing!

You can get polyester stuffing from any craft store, or even many grocery stores!

[![Step Six]({static}/files/cat_pillow_tutorial/thumb-6.jpg)]({static}/files/cat_pillow_tutorial/step6.jpg)

Once it's stuffed, pin it closed, to aid in sewing in the next step.

[![Step Seven]({static}/files/cat_pillow_tutorial/thumb-7.jpg)]({static}/files/cat_pillow_tutorial/step7.jpg)

And then, sew it shut!

[![Step Eight]({static}/files/cat_pillow_tutorial/thumb-8.jpg)]({static}/files/cat_pillow_tutorial/step8.jpg)

And finally, remove the pin and let your little rascal play with it!

[![Bindy Playing]({static}/files/cat_pillow_tutorial/thumb-0.jpg)]({static}/files/cat_pillow_tutorial/bindy_playing.jpg)

Don't feel like buying fabric, sewing, and stuffing? Well, [you can purchase pre-made pillows from us!](https://www.etsy.com/shop/WorldsProject?section_id=21264586) 
